import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
//import{FormGroup} from '@angular/forms'
import { AppComponent } from './app.component';
//import{FormControl} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ContactComponent } from './components/contact/contact.component';
import { DropComponent } from './drop/drop.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { AppRoutingModule} from '../app/app-routing.module';
import { ButtonComponent } from './button/button.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AlertComponent } from './alert/alert.component';






export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    DropComponent,
    ButtonComponent,
    NavbarComponent,
   AlertComponent,
   
   
  ],
  imports: [
    BrowserModule, ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  
  
    TranslateModule.forRoot({ 
      //defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
