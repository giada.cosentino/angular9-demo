import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GenerateErrorService } from '../services/generate-error.service';
import { HandleErrorsService } from '../services/handle-errors.service';
import { AppError } from '../services/AppError';
import {SharedServicesService} from '../services/shared-services.service';



@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {


  errore: AppError;

  //@Output() errorEmitter = new EventEmitter<AppError>(); 

  constructor(private backend: GenerateErrorService, private errorService: HandleErrorsService, private sharedServices: SharedServicesService) { }

  ngOnInit(): void {
  }
  


  doCall(num){
   this.backend.call(''+num).subscribe(
     risp=>console.log(risp),
     err=>{this.show(this.errorService.handle(err, "contacts"));}     
     );
  }

  show(err: AppError): void {
    console.log(err);
    //this.errorEmitter.emit(err);
    //this.errore=err;
    this.sharedServices.nextMessage(err);
  }

}