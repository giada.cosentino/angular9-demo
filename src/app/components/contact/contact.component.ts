import { Component, OnInit, Output, EventEmitter, HostListener, HostBinding } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { SideBarService } from '../../services/side-bar.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})

export class ContactComponent implements OnInit {
  @HostBinding('class.is-open')

  myform: FormGroup;

  firstName: AbstractControl;
  lastName: AbstractControl;
  email: AbstractControl;
  password: AbstractControl;
  language: FormControl;

  isOpen = false;
  constructor(private sideBarService: SideBarService, private fb: FormBuilder, public translate: TranslateService) {    
    this.createForm();
    translate.addLangs(['en', 'es','it']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|it/) ? browserLang : 'en');
  }


  ngOnInit() {    
    
    this.sideBarService.change.subscribe(isOpen => {
      this.isOpen = isOpen;
    });

  }


  createForm() {    
    this.myform =
    this.fb.group({
      firstName:'',
      lastName:'',
      email:'',
      password:''
    })
    this.myform.get('lastName').setValue('pippo');

    this.firstName = this.myform.get('firstName');
    this.lastName = this.myform.get('lastName');
    this.email = this.myform.get('email');
    this.password = this.myform.get('password');
  }


  onSubmit() {
    console.log("submitted!")
    if (this.myform.valid) {
      console.log("Form Submitted!");
      this.myform.reset();
    }
  }

  invio() {
    console.log("invio", this.email.value)
    if (this.myform.valid) {
      console.log(this.myform.value)
    } else {
      console.error("Form non valida");
    }
    
  }

  
  

}
