import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import{ButtonComponent} from '../app/button/button.component'
import { ContactComponent } from './components/contact/contact.component';
import { DropComponent } from './drop/drop.component';
import {AlertComponent} from '../app/alert/alert.component';

const routes: Routes = [
  { path: '', redirectTo:'/home', pathMatch: 'full'},
  { path: 'home', component:DropComponent },
  { path: 'button', component:ButtonComponent},
  { path: 'contact', component:ContactComponent },
  { path: '**', component:ContactComponent },
  { path: 'alert', component:AlertComponent }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes),],

  exports:[RouterModule]  

  
})
export class AppRoutingModule { }
