import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import {EventEmitter } from '@angular/core';
import { AppError } from './AppError';

@Injectable({
  providedIn: 'root'
})
export class SharedServicesService {

  private messageSource = new BehaviorSubject<AppError>(null);
 
  sharedMessage = this.messageSource.asObservable();

  constructor() { }

  nextMessage(message: AppError) {
    console.log("shared service", message)
    this.messageSource.next(message)
  }
}
