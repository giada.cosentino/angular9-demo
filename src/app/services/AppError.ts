export class AppError {
  code: number;
  level: string;
  title: string;
  message: string;
}