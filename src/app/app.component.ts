import { Component, OnInit, HostListener } from '@angular/core';
import { AppError } from './services/AppError';
import { SideBarService } from '../app/services/side-bar.service'
import { TranslateService } from '@ngx-translate/core';
import { AbstractControl } from '@angular/forms';
import { StorageService } from '../app/services/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor(
    private sideBarService: SideBarService,
    private translate: TranslateService,
    private ls: StorageService,
    private router: Router
  ) {
    translate.addLangs(['en', 'es', 'it']);
  }

  errorMessage: AppError;
  doErrorEmitter($event): void {
    console.log($event)
    this.errorMessage = $event;
  }
  ngOnInit() {
    const browserLang = this.translate.getBrowserLang();
    console.log("browserLang: ", browserLang);
    // codice che guarderà se è presente una preferenza di lingua oppure no
    const preferredLang = this.ls.getData('language');
    console.log(preferredLang);
    if (preferredLang != null) {
      this.settaLingua(preferredLang)
    } else {
      this.settaLingua(browserLang);
    }
  }

  settaLingua(codice: string): void {
    console.log("setto la lingua:" + codice);
    this.translate.use(codice.match(/en|es|it/) ? codice : 'en');
  }

  @HostListener('click')
  click() {
    this.sideBarService.toggle();
  }


}
