import { Component, OnInit, HostListener } from '@angular/core';
import { SideBarService } from '../services/side-bar.service'
import { TranslateService } from '@ngx-translate/core';
import { AbstractControl } from '@angular/forms';
import { StorageService } from '../services/storage.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-drop',
  templateUrl: './drop.component.html',
  styleUrls: ['./drop.component.css'],
})
export class DropComponent implements OnInit {

  constructor(
    //private sideBarService: SideBarService,
    public translate: TranslateService,
    private ls: StorageService,
    //private router: Router
  ) {
    /*translate.addLangs(['en', 'es', 'it']);*/
    //translate.setDefaultLang('en');
  }
  /*gotoHome(){
    this.router.navigate(['/home']); }*/
  ngOnInit() {/*
    const browserLang = this.translate.getBrowserLang();
    console.log("browserLang: ", browserLang);
    // codice che guarderà se è presente una preferenza di lingua oppure no
    const preferredLang = this.ls.getData('language');
    console.log(preferredLang);
    if (preferredLang!=null) {
      this.settaLingua(preferredLang)
    } else {
      this.settaLingua(browserLang);
    }
  */ }

 /* @HostListener('click')
  click() {
    this.sideBarService.toggle();
  }*/

  settaLingua(codice: string): void {
    //throw err;
    console.log("setto la lingua:" +codice);
    this.translate.use(codice.match(/en|es|it/) ? codice : 'en');
  }
  
  cambiaLingua(menu: AbstractControl): void {
    console.log('cambiaLingua: ',menu.value);
    this.ls.setData('language',menu.value);
    this.settaLingua(menu.value);
  }


}
